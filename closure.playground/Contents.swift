//Написать сортировку массива с помощью замыкания, сначала в одну сторону, затем в другую. Вывести всё в консоль.
let numbers = [5, 4, 11, 8, 25, 9]

let firstSort = numbers.sorted {$0 > $1}
let secondSort = numbers.sorted {$0 < $1}

print(firstSort)
print(secondSort)

//Создать метод, который принимает имена друзей, после этого имена положить в массив. Массив отсортировать по количеству букв в имени.
var names = [String]()

func nameOut (name: String) {
    names.append(name)
    let friends = names.sorted(by: {$0.count < $1.count})
    print(friends)
}

nameOut(name: "Mark")
nameOut(name: "Yo")

//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга. Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение.
let dict: [Int: String] = [3: "Bro", 4: "Alex", 5: "Anton"]

func dictOut (key: Int) {
    print("\(key): \(dict[key]!)")
}

dictOut(key: 3)
dictOut(key: 4)

//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль.
var numArr = [Int]()
var strArr = [String]()

func arrOut (num: inout [Int], str: inout [String]) {
    if num.isEmpty {
        num.append(10)
    }
    if str.isEmpty {
        str.append("Hi")
    }
    for i in num {
        print(i)
    }
    for i in str {
        print(i)
    }
}

arrOut(num: &numArr, str: &strArr)
